/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { BadRequestException, ConflictException, Injectable,NotFoundException, UnauthorizedException, UploadedFiles } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId, Query, isValidObjectId } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as speakeasy from 'speakeasy';
import { JwtService } from '@nestjs/jwt';
import {ConfigService } from '@nestjs/config'
import { SignUpDto } from './dto/signup.dto';
import { LoginDto } from './dto/login.dto';
import { ResetPasswordDemandDto } from './dto/resetPasswordDemand.dto';
import { MailerService } from 'src/mailer/mailer.service';
import { ResetPasswordConfirmationDto } from './dto/resetPasswordConfirmation.dto';
import { User } from 'src/schemas/user.schema';
import { UpdateUserDto } from './dto/update.dto';
import { FormExpertDto } from './dto/formexpert.dto';
import { Expert } from 'src/schemas/expert.schema';
import multer, { diskStorage } from 'multer';
import path, { extname } from 'path';
import { CarPost } from 'src/schemas/carpost.schema';
import { UpdatePasswordDto } from './dto/updatePassword.dto';
import { ResetPasswordCodeDto } from './dto/resetPasswordCode';
import { TDocumentDefinitions } from 'pdfmake/interfaces';
import PdfPrinter = require('pdfmake');


@Injectable()
export class AuthService {
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<User>,
        @InjectModel(Expert.name) private readonly expertModel: Model<Expert>,
        @InjectModel(CarPost.name) private readonly carPostModel: Model<CarPost>,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
        private readonly emailService : MailerService,
    ) {}

    

    async signUp(signUpDto: SignUpDto): Promise<{ token: string, data: string , newUser :{userId: string,role:string, expert?: Expert}}> {
        const { name, email, password, phone, address } = signUpDto;
        const existingUserWithEmail = await this.userModel.findOne({ email });
        if (existingUserWithEmail) {
            throw new ConflictException('Email déjà utilisé');
        }
        
        const existingUserWithName = await this.userModel.findOne({ name });
        if (existingUserWithName) {
            throw new ConflictException("Nom d'utilisateur déjà utilisé");
        }
    
        const existingExpertRequest = await this.expertModel.findOne({ email });
        const role = existingExpertRequest ? 'expert' : 'user';
    
        const hashedPassword = await bcrypt.hash(password, 10);
        const user = await this.userModel.create({
            name,
            email,
            password: hashedPassword,
            phone,
            address,
            role: role
        });
        user.save();
    
        const token = this.jwtService.sign({ id: user._id });
    
        let expert: Expert | undefined;
        if (role === 'expert') {
            expert = existingExpertRequest;
        }
    
        return {
            data: "User successfully created",
            token,
            newUser: {
                userId: user._id.toString(),
                role,
                expert 
            }
        };
    }
    
    async login(loginDto: LoginDto): Promise<{ token: string,  user: {name: string, email: string , userId: string, role: string, expert?: Expert} }> {
        const { email, password } = loginDto;
    
        const user = await this.userModel.findOne({ email });
        if (!user) {
            throw new UnauthorizedException('Email ou mot de passe invalide');
        }
    
        const isPasswordMatched = await bcrypt.compare(password, user.password);
        if (!isPasswordMatched) {
            throw new UnauthorizedException('Mot de passe ne correspond pas');
        }
    
        const existingExpertRequest = await this.expertModel.findOne({ email });
        const role = existingExpertRequest ? 'expert' : 'user';
    
        const token = this.jwtService.sign({ id: user._id });
    
        let expert: Expert | undefined;
        if (role === 'expert') {
            expert = existingExpertRequest;
        }
    
        return {
            token,
            user: {
                name: user.name,
                email: user.email,
                userId: user._id.toString(),
                role,
                expert 
            }
        };
    }
    

    async getUserById(id: string): Promise<User | null> {
        return await this.userModel.findById(id)
        .populate('demandes.user', '_id name email')
        .populate('demandes.post', '_id');
    }
    
    
    async getExpertById(id: string): Promise<Expert | null> {
        return await this.expertModel.findById(id);
    }
    

    async getData(){
        const account = await this.userModel.find()
        return account;
        
    }

    async resetPasswordDemand(resetPasswordDemandDto: ResetPasswordDemandDto): Promise<{data: string,code :string}> {
        const { email } = resetPasswordDemandDto;
        const user = await this.userModel.findOne({ email });
        if (!user) {
            throw new NotFoundException('Utilisateur non trouvé');
        }
    
        const code = speakeasy.totp({
            secret: this.configService.get("OTP_CODE"),
            digits: 5,
            step: 60 * 15, // 15 minutes
            encoding: "base32"
        });
    
        const url = "http://localhost:3000/motdepasseoublié"
        await this.emailService.sendResetPassword(email, url, code);
        
        const token = this.jwtService.sign({ id: user._id });
    
        return {data: "Reset password mail has been sent", code };
    }

    // async resetPasswordCode(resetPasswordConfirmationDto: ResetPasswordCodeDto): Promise<{ token: string, data: string }> {
    //     const {email,code}=resetPasswordConfirmationDto
        
    //     const user = await this.userModel.findOne({ email });
    //     if (!user) {
    //         throw new NotFoundException('Utilisateur non trouvé');
    //     }
    //     const match=speakeasy.totp.verify({
    //         secret: this.configService.get('OTP_CODE'),
    //         token: code,
    //         digits:5,
    //         step:60 * 15,
    //         encoding: "base32",
    //     });
        
    //     if(!match) throw new UnauthorizedException ("Invalid/expired token")
        
    //     const token = this.jwtService.sign({ id: user._id });
    
    //     return { token, data: "Code sent"};
    // }

    async resetPasswordConfirmation(resetPasswordConfirmationDto: ResetPasswordConfirmationDto): Promise<{ token: string, data: string }> {
        const {email,password,code}=resetPasswordConfirmationDto
        
        const user = await this.userModel.findOne({ email });
        if (!user) {
            throw new NotFoundException('Utilisateur non trouvé');
        }
        const match=speakeasy.totp.verify({
            secret: this.configService.get('OTP_CODE'),
            token: code,
            digits:5,
            step:60 * 15,
            encoding: "base32",
        });
        
        if(!match) throw new UnauthorizedException ("Invalid/expired token")
            const hash= await bcrypt.hash(password, 10)
        await this.userModel.updateOne(
            { email }, 
            { $set: { password: hash } } 
        );
        
        const token = this.jwtService.sign({ id: user._id });
    
        return { token, data: "Password updated"};
    }

    
    async updateUser(id: string, updateUserDto: UpdateUserDto ) {
        const user = await this.userModel.findById(id);
        if (!user) {
            throw new NotFoundException('User not found');
        }
        Object.assign(user, updateUserDto);
        const updatedUser = await user.save();
        
        return updatedUser;
    }

    async updateUserPassword(id: string, updatePasswordDto: UpdateUserDto ) {
        const { password, newPassword, confirmPassword } = updatePasswordDto;
    
        if (newPassword !== confirmPassword) {
            throw new BadRequestException('Pas identiques');
        }
    
        const user = await this.userModel.findById(id);
        if (!user) {
            throw new NotFoundException('User not found');
        }

        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            throw new BadRequestException('Mot de passe actuel incorrect');
        }

        const hashedNewPassword = await bcrypt.hash(newPassword, 10); 
        user.password = hashedNewPassword;
        const updatedUser = await user.save();
    
        return {data :"Mot de passe changé",updatedUser};
    }
    


    async demandExpert(formExpertDto: FormExpertDto, certif: Express.Multer.File, userEmail?: string): Promise<{ data: string, expert?: Expert }> {
        let data: string;
        let expert: Expert | undefined;
    
        try {
            const uploadResponse = await this.uploadCertifExp(certif);
            const fileName = uploadResponse.fileName;
    
            const { name, email, phone, address, role } = formExpertDto;
    
            if (userEmail) {
                expert = await this.expertModel.create({
                    name,
                    email: userEmail, 
                    phone,
                    address,
                    certif: fileName,
                    role
                });
            } else {
                expert = await this.expertModel.create({
                    name,
                    email,
                    phone,
                    address,
                    certif: fileName,
                    role
                });
            }
    
            if (userEmail) {
                await this.userModel.updateOne({ email: userEmail }, { $set: { role: 'expert' } });
    
                const user = await this.userModel.findOne({ email: userEmail });
                if (!user) {
                    throw new NotFoundException('Utilisateur non trouvé');
                }
    
                user.demandes = expert.demandes;
                user.expertCertif = expert.certif;
                await user.save();
            }
    
            data = "Demande créée avec succès";
        } catch (error) {
            data = "Erreur dans la création de la demande ";
            throw error;
        }
    
        return { data, expert };
    }
    
    
    
    

    async uploadCertifExp(file: Express.Multer.File) {
        try {
            const fileName = `${(file.originalname)}`;
            // const originalFileName = path.parse(file.originalname).name; 
            // const fileName = `${originalFileName}-${Date.now()}-${Math.round(Math.random() * 1E9)}${path.extname(file.originalname)}`;
            return { success: true, message: "Fichier téléchargé avec succès", fileName };
        } catch (error) {

            return { success: false, message: "Une erreur est survenue lors du téléchargement du fichier", error };
        }
    }


    async getExperts() {
        return this.expertModel.find().exec();
    }



    async updateDemandesInUser(userId: string, demande: { user: string, post: string }): Promise<void> {
        try {
            const user = await this.userModel.findById(userId);
            if (!user) {
                throw new NotFoundException('Utilisateur non trouvé');
            }
    
            user.demandes.push(demande);
            await user.save();
            console.log('Demandes de l\'utilisateur mises à jour avec succès');
        } catch (error) {
            console.error('Erreur lors de la mise à jour des demandes de l\'utilisateur :', error);
        }
    }
    
    
    async chooseExpertById(expertId: string, postId: string, userId: string): Promise<Expert | null> {
        try {
            const chosenExpert = await this.expertModel.findById(expertId);
            if (!chosenExpert) {
                throw new NotFoundException('Expert non trouvé');
            }
    
            const post = await this.carPostModel.findById(postId);
            if (!post) {
                throw new NotFoundException('Post non trouvé');
            }
    
            const demande = {
                user: userId,
                post: postId,
            };
    
            chosenExpert.demandes.push(demande);
            await chosenExpert.save();
    
            await this.updateDemandesInUser(userId, demande);
    
            await this.emailService.demandExpertise(chosenExpert.email);
            console.log("Email envoyé avec succès");
    
            return chosenExpert;
        } catch (error) {
            console.error('Erreur lors du choix de l\'expert :', error);
            return null;
        }

    }


    async acceptOrRejectDemand(expertId: string, postId: string, userId: string, accepted: boolean): Promise<Expert | null> {
        try {
            const chosenExpert = await this.expertModel.findById(expertId);
            if (!chosenExpert) {
                throw new NotFoundException('Expert non trouvé');
            }
    
            const post = await this.carPostModel.findById(postId);
            if (!post) {
                throw new NotFoundException('Post non trouvé');
            }
    
            const demande = {
                user: userId,
                expert: expertId,
                post: postId,
                accepted: accepted,
            };
    
            await this.updateDemandesInUser(userId, demande);
    
            if (accepted) {
                chosenExpert.demandesAcceptees.push(demande);
                await this.emailService.demandAccepted(chosenExpert.email);

            } else {
                chosenExpert.demandesRefusees.push(demande);
                await this.emailService.demandRejected(chosenExpert.email);

            }
            await chosenExpert.save();
    
    
            return chosenExpert;
        } catch (error) {
            console.error('Erreur lors de l\'acceptation ou du refus de la demande d\'expertise :', error);
            return null;
        }
    }


    async createPdfdoc(): Promise<PDFKit.PDFDocument> {
            const fonts = {
                Roboto: {
                    normal: 'fonts/Roboto-Regular.ttf',
                    bold: 'fonts/Roboto-Medium.ttf',
                    italics: 'fonts/Roboto-Italic.ttf',
                    bolditalics: 'fonts/Roboto-MediumItalic.ttf',
                },
            };

            const printer = new PdfPrinter(fonts);

            const docDefinition = {
                content: [
                "First paragraph",
                "Rapport d'expertise"
                ]
            } as TDocumentDefinitions;

            const options = {
                // ...
            };

            return printer.createPdfKitDocument(docDefinition, options);
        }


    }

