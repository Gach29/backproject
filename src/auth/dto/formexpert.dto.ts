/* eslint-disable prettier/prettier */
import { IsEmail, IsNotEmpty,IsString, Matches } from "class-validator";


export class FormExpertDto {
    @IsNotEmpty({ message : "Nom est obligatoire"})
    @IsString()
    @Matches(/^[a-zA-Z' ']+$/, { message: "Nom invalide, utilisez uniquement des lettres" })
    readonly name: string;


    @IsNotEmpty({ message : "Email est obligatoire"})
    @IsEmail({}, { message : "Email invalide"})
    readonly email: string;


    @IsNotEmpty({ message: "Numéro de téléphone est obligatoire" })
    @Matches(/^[0-9]{8}$/, { message: "Numéro de téléphone invalide"})
    readonly phone: string;

    @IsNotEmpty({ message : "Adresse est obligatoire"})
    readonly address: string;

    readonly certif: string;

    readonly role: string;

}

