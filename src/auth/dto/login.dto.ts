/* eslint-disable prettier/prettier */
import { IsEmail, IsNotEmpty, IsString} from "class-validator";


export class LoginDto {

    @IsNotEmpty({ message : "Email est obligatoire"})
    @IsEmail({}, { message : "Email invalide"})
    readonly email: string;

    @IsNotEmpty({ message : "Mot de passe est obligatoire"})
    @IsString()
    readonly password: string;


}