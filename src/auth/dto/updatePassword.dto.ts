/* eslint-disable prettier/prettier */
import { IsNotEmpty, IsString, MinLength} from 'class-validator';

export class UpdatePasswordDto {
    @IsNotEmpty()
    @IsString()
    readonly oldPassword: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(6)
    readonly newPassword: string;

    @IsNotEmpty()
    @IsString()
    readonly confirmPassword: string;

    // Custom validation function to check if newPassword and confirmPassword match
    validatePasswordConfirmation() {
        if (this.newPassword !== this.confirmPassword) {
            throw new Error('Les mots de passe ne correspondent pas');
        }
    }
}