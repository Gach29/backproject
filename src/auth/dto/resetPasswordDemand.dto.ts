/* eslint-disable prettier/prettier */
import { IsEmail, IsNotEmpty } from "class-validator";


export class ResetPasswordDemandDto {

    @IsNotEmpty()
    @IsEmail({}, { message : "Email invalide"})
    readonly email: string;


}