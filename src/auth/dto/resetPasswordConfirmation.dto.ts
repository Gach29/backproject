/* eslint-disable prettier/prettier */
import { IsEmail, IsNotEmpty, IsString, MaxLength, MinLength } from "class-validator";


export class ResetPasswordConfirmationDto{
    @IsNotEmpty({ message : "Email est obligatoire"})
    @IsEmail({}, { message : "Email invalide"})
    readonly email: string;

    @IsNotEmpty({ message : "Mot de passe est obligatoire"})
    @IsString()
    @MinLength(6, { message :"Mot de passe doit comporter au moins 6 caractères"})
    @MaxLength(15, { message: "Mot de passe est trop long" })
    readonly password: string;
    

    @IsNotEmpty({ message : "Code est obligatoire"})
    readonly code : string

}