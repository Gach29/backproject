/* eslint-disable prettier/prettier */


/* eslint-disable prettier/prettier */
export class UpdateUserDto {
    name: string; 
    email: string; 
    password: string;
    phone: string; 
    address: string;
    profileImage:string;
    newPassword: string;
    confirmPassword: string;

    validatePasswordConfirmation() {
        if (this.newPassword !== this.confirmPassword) {
            throw new Error('Les mots de passe ne correspondent pas');
        }
    }
    
}