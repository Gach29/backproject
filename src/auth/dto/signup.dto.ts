/* eslint-disable prettier/prettier */
import { IsEmail, IsNotEmpty,IsString, Matches, MaxLength, MinLength } from "class-validator";


export class SignUpDto {
    @IsNotEmpty({ message : "Nom est obligatoire"})
    @IsString()
    @Matches(/^[a-zA-Z' ']+$/, { message: "Nom invalide, utilisez uniquement des lettres" })
    readonly name: string;


    @IsNotEmpty({ message : "Email est obligatoire"})
    @IsEmail({}, { message : "Email invalide"})
    readonly email: string;


    @IsNotEmpty({ message : "Mot de passe est obligatoire"})
    @IsString()
    @MinLength(6, { message :"Mot de passe doit comporter au moins 6 caractères"})
    @MaxLength(15, { message: "Mot de passe est trop long" })
    readonly password: string;


    @IsNotEmpty({ message: "Numéro de téléphone est obligatoire" })
    @Matches(/^[0-9]{8}$/, { message: "Numéro de téléphone invalide"})
    readonly phone: string;

    readonly address?: string;

    readonly role:string;
}

