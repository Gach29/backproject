/* eslint-disable prettier/prettier */
import { IsEmail, IsNotEmpty} from "class-validator";


export class ResetPasswordCodeDto{
    @IsNotEmpty({ message : "Email est obligatoire"})
    @IsEmail({}, { message : "Email invalide"})
    readonly email: string;

    

    @IsNotEmpty({ message : "Code est obligatoire"})
    readonly code : string

}