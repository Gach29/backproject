/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { BadRequestException, Body, Controller, Get, HttpStatus, NotFoundException, Param, Post, Put, Query, Req, Res, UploadedFile, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { ResetPasswordDemandDto } from './dto/resetPasswordDemand.dto';
import { ResetPasswordConfirmationDto } from './dto/resetPasswordConfirmation.dto';
import { SignUpDto } from './dto/signup.dto';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Response } from 'express';
import { extname } from 'path';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { UpdateUserDto } from './dto/update.dto';
import { AuthGuard } from '@nestjs/passport';
import { Observable, catchError, from, map, of, tap } from 'rxjs';
import path = require('path');
import {Query as QuerySearch} from 'express-serve-static-core'
import { User } from 'src/schemas/user.schema';
import { FormExpertDto } from './dto/formexpert.dto';
import { Expert } from 'src/schemas/expert.schema';
import { UpdatePasswordDto } from './dto/updatePassword.dto';
import { ResetPasswordCodeDto } from './dto/resetPasswordCode';


export const storage={
    storage: diskStorage({
        destination: './uploads/expertCertif',
        filename: (req, file, cb) => {
            const originalFileName = path.parse(file.originalname).name; 
            const uniqueFileName = `${originalFileName}-${Date.now()}-${Math.round(Math.random() * 1E9)}${path.extname(file.originalname)}`;
            cb(null, uniqueFileName);
        },
        }),
        fileFilter: (req, file, cb) => {
            const ext = extname(file.originalname).toLowerCase();
            if (ext === '.pdf' || ext === '.docx' || ext === '.jpg' || ext === '.jpeg' || ext === '.png') {
                cb(null, true);
            } else {
                cb(new BadRequestException('Seuls les fichiers de type PDF, Word, JPG ou PNG sont autorisés'),false);
            }
        }
    }
    

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService,
    ) {}

    @Post('/signup')
    async signUp(@Body() signUpDto: SignUpDto): Promise<{ token: string }> {
        return this.authService.signUp(signUpDto);
    }

    @Post('/login') 
    login(@Body() loginDto: LoginDto): Promise<{ token: string }> {
        return this.authService.login(loginDto);
    }

    @Get('/users/:id')
    async getUserById(@Param('id') id: string) {
        return this.authService.getUserById(id);
    }
    
    @Get('/experts/:id')
    async getExpertById(@Param('id') id: string) {
        return this.authService.getExpertById(id);
    }
    
    @Get('')
    getData() {
        return this.authService.getData(); 
    }


    @UseGuards(AuthGuard("jwt"))
    @Post('/upload')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: './uploads',
            filename: (req, file, cb) => {
                const originalFileName = path.parse(file.originalname).name; 
                const uniqueFileName = `${originalFileName}-${Date.now()}-${Math.round(Math.random() * 1E9)}${path.extname(file.originalname)}`;
                cb(null, uniqueFileName);
            },
        }),
        fileFilter: (req, file, cb) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
                return cb(new Error("Le fichier n'est pas une image"), false);
                }
            cb(null, true);
                },
            limits: {
                fileSize: 1024 * 1024 * 10 
            }
    }))
    // eslint-disable-next-line @typescript-eslint/ban-types
    UploadFile(@UploadedFile() file: Express.Multer.File, @Req() req): Observable<Object> {
        const user = req.user;
        if (!user) {
            return of({ success: false, message: "Utilisateur non trouvé" });
        }
            user.profileImage= file.filename; 
            return from(user.save()).pipe(
                map(() => ({ success: true, message: "Fichier téléchargé avec succès" })),
                catchError(async (error) => ({ success: false, message: "Une erreur est survenue lors du téléchargement du fichier", error }))
            );
        
    }

    
    @Get('pictures/:filename')
    // eslint-disable-next-line @typescript-eslint/ban-types
    getPicture(@Param('filename') filename , @Res() res) :Observable<Object> {
        return of(res.sendFile(filename, { root: './uploads' }));
    }


    @Post('/reset-password')
    resetPasswordDemand(@Body() resetPasswordDemandDto: ResetPasswordDemandDto) : Promise<{ code : string}>{
        return this.authService.resetPasswordDemand(resetPasswordDemandDto);
    }

    @Post('/reset-password-confirmation')
    resetPasswordConfirmation(@Body() resetPasswordConfirmationDto: ResetPasswordConfirmationDto) : Promise<{ token: string }>{
        return this.authService. resetPasswordConfirmation(resetPasswordConfirmationDto);
    }

    // @Post('/reset-password-code')
    // resetPasswordCode(@Body() resetPasswordConfirmationDto: ResetPasswordCodeDto) : Promise<{ token: string }>{
    //     return this.authService. resetPasswordCode(resetPasswordConfirmationDto);
    // }

    @Put(':id')
    updateUser(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
        const updatedUser = this.authService.updateUser(id, updateUserDto);
        if (!updatedUser) {
            throw new NotFoundException('User not found');
        }
        return updatedUser;
    }

    @Put(':id/password')
    async updateUserPassword(@Param('id') id: string, @Body() updatePasswordDto: UpdateUserDto) {
        return await this.authService.updateUserPassword(id, updatePasswordDto);
    }


    @Post('/expertdemand')
    @UseInterceptors(FileInterceptor('certif'))
    async demandExpert(@UploadedFile() certif: Express.Multer.File, @Body() formExpertDto: FormExpertDto) {
        return await this.authService.demandExpert(formExpertDto, certif);

    }


    @Post('/uploadexp')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: './uploads/expertCertif',
            filename: (req, file, cb) => {
                const uniqueFileName = `${Date.now()}-${Math.round(Math.random() * 1E9)}${extname(file.originalname)}`;
                cb(null, uniqueFileName);
            },
        }),
        fileFilter: (req, file, cb) => {
            const ext = extname(file.originalname).toLowerCase();
            if (ext === '.pdf' || ext === '.docx' || ext === '.jpg' || ext === '.jpeg' || ext === '.png') {
                cb(null, true);
            } else {
                cb(new BadRequestException('Seuls les fichiers de type PDF, Word, JPG ou PNG sont autorisés'), false);
            }
        }
    }))
    async uploadCertifExp(@UploadedFile() file: Express.Multer.File) {
        try {
            return { success: true, message: "Fichier téléchargé avec succès", fileName: file.filename };
        } catch (error) {
            return { success: false, message: "Une erreur est survenue lors du téléchargement du fichier", error };
        }
    }


    @UseGuards(AuthGuard('jwt'))
    @Post('/uploadcertif')
    @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
        destination: './uploads/expertCertif',
        filename: (req, file, cb) => {
            const originalFileName = path.parse(file.originalname).name; 
            const uniqueFileName = `${originalFileName}-${Date.now()}-${Math.round(Math.random() * 1E9)}${path.extname(file.originalname)}`;
            cb(null, uniqueFileName);
        },
        }),
        fileFilter: (req, file, cb) => {
            const ext = extname(file.originalname).toLowerCase();
            if (ext === '.pdf' || ext === '.docx' || ext === '.jpg' || ext === '.jpeg' || ext === '.png') {
                cb(null, true);
            } else {
                cb(new BadRequestException('Seuls les fichiers de type PDF, Word, JPG ou PNG sont autorisés'),false);
            }
        }
    }))
    async uploadCertif(@UploadedFile() file: Express.Multer.File, @Req() req) {
        try {
            const user = req.user; 
            if (!user) {
            return { success: false, message: "Utilisateur non trouvé" };
            }
            user.expertCertif = file.filename;
            
            await user.save();
            return { success: true, message: "Fichier téléchargé avec succès" };
        } catch (error) {
            return { success: false, message: "Une erreur est survenue lors du téléchargement du fichier", error };
        }
    }

    @Get('/experts')
    async getExperts() {
        return this.authService.getExperts();
    }

    

    @Post('/choix')
    @UseGuards(AuthGuard('jwt'))
    async chooseExpertById(@Body() body: { expertId: string, postId: string }, @Req() req: any): Promise<{ message: string, expert: Expert | null }> {
        const userId = req.user.id; 
        const chosenExpert = await this.authService.chooseExpertById(body.expertId, body.postId, userId);
        if (chosenExpert) {
            return { message: "Expert choisi avec succès", expert: chosenExpert };
        } else {
            return { message: "Aucun expert trouvé avec cet ID", expert: null };
        }
    }
    


    @Post('choix/status')
    async acceptOrRejectDemand(@Body() body: any) {
        try {
            const { expertId, postId, userId, accepted } = body;
            const chosenExpert = await this.authService.acceptOrRejectDemand(expertId, postId, userId, accepted);
            if (!chosenExpert) {
                return { error: "Expert non trouvé" };
            }
            return chosenExpert;
        } catch (error) {
            console.error("Erreur lors de la gestion de la demande :", error);
            return { error: "Erreur lors de la gestion de la demande" };
        }
    }

    @Get("/expertPdf")
    async getPDFDoc(@Res() res: Response) {
        try {
            const PDFDoc = await this.authService.createPdfdoc();
            PDFDoc.pipe(res);
            PDFDoc.end();
        } catch (error) {
            console.error("Error generating PDF:", error);
            res.status(500).send("Error generating PDF");
        }
    }
    

}
        


 // @UseGuards(AuthGuard('jwt'))
    // @Post('/upload-photo')
    // @UseInterceptors(FilesInterceptor('image', 10, {
    //     storage: diskStorage({
    //         destination: './uploads',
    //         filename: (req, file, cb) => {
    //             const name = file.originalname.split('.')[0];
    //             const extension = extname(file.originalname);
    //             const newFileName = `${name}_${Date.now()}${extension}`;
    //             cb(null, newFileName);
    //         }
    //     }),
    //     fileFilter: (req, file, cb) => {
    //         if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
    //             return cb(new Error("Le fichier n'est pas une image"), false);
    //         }
    //         cb(null, true);
    //     },
    //     limits: {
    //         fileSize: 1024 * 1024 * 10 
    //     }
    // }))
    // async uploadPhoto(@UploadedFiles() files: Express.Multer.File[], @Req() req) {
    //     if (!files || files.length === 0) {
    //         throw new BadRequestException("Aucun fichier n'a été envoyé");
    //     }
    
    //     const userId = req.user._id; 

    //     const uploadedFiles = [];
    //     const fileUrls = [];

    //     for (const file of files) {
    //         const uploadedFile = {
    //             originalName: file.originalname,
    //             destination: file.destination,
    //             filename: file.filename,
    //             path: file.path, 
    //             size: file.size,
    //             userId: userId 
    //         };

    //         const fileUrl = `http://localhost:8000/auth/pictures/${file.filename}`;
            
    //         uploadedFiles.push(uploadedFile);
    //         fileUrls.push(fileUrl);
    //     }

    //     await this.uploadedImageModel.create(uploadedFiles);
        
    
    //     return {
    //         message: 'Fichier(s) téléchargé(s) avec succès',
    //         files: uploadedFiles,
    //         fileUrls
    //     };
    // }


//     @Get('/uploaded-images')
//     async getUploadedImages() {
//     const images = await this.uploadedImageModel.find().exec();
//     return images.map(image => image.filename);
// }
