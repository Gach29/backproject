/* eslint-disable prettier/prettier */
import { WebSocketGateway, WebSocketServer, OnGatewayInit } from '@nestjs/websockets';
import { Server } from 'socket.io';

@WebSocketGateway()
export class NotificationGateway implements OnGatewayInit {
    @WebSocketServer() server: Server;

    afterInit() {
        console.log('Socket.IO initialized');
    }

    sendNotification(notification: any) {
        this.server.emit('newNotification', notification);
    }
}
