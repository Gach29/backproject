/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('notifications')
export class NotificationController {
    constructor(private readonly notificationService: NotificationService) {}

    @Post()
    async createNotification(@Body() createNotificationDto: any) {
        const { userId, user, content, postId} = createNotificationDto;
        return this.notificationService.createNotification(userId, user ,content,postId);
    }

    @Get()
    async getNotifications(){
        return this.notificationService.getNotifications();
    }

    @Get(':userId')
    async getNotificationsByUser(@Param('userId') userId: string){
        return this.notificationService.getNotificationsByUser(userId);
    }

    @UseGuards(AuthGuard('jwt')) 
    @Delete(':notificationId')
    async deleteNotificationByUser(@Param('notificationId') notificationId: string, @Req() request) {
        try {
        const userId = request.user.id; 
        await this.notificationService.deleteNotification(userId, notificationId);
        return { message: 'Notification supprimée avec succès.' };
        } catch (error) {
        throw new Error('Erreur lors de la suppression de la notification.');
        }
    }
}
