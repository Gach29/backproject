/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Notification } from 'src/schemas/notifications.schema';

@Injectable()
export class NotificationService {

    constructor(@InjectModel(Notification.name) private readonly notificationModel: Model<Notification>) {}

    async createNotification(Id: string, user: string, content: string, postId: string): Promise<Notification> {
        const newNotification = new this.notificationModel({ Id,user,content,postId });
        return newNotification.save();
        
    }

    async getNotifications() {
        return this.notificationModel.find();
    }

    async getNotificationsByUser(userId: string) {
        const notifications = await this.notificationModel.find({ user: userId }).exec();
        return notifications;
    }

    async deleteNotification(userId: string, notificationId: string){
        try {
            await this.notificationModel.deleteOne({ user: userId, _id: notificationId });
        } catch (error) {
            throw new Error('Erreur lors de la suppression de la notification.');
        }
    }
}
