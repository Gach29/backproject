/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CarPost, CarPostSchema } from 'src/schemas/carpost.schema';
import { User, UserSchema } from 'src/schemas/user.schema';
import { JwtModule } from '@nestjs/jwt'; 
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from 'src/auth/auth.module';
import { Comment, CommentSchema } from 'src/schemas/comments.schema';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { Notification, NotificationSchema } from 'src/schemas/notifications.schema';
import { NotificationService } from 'src/notification/notification.service';

@Module({
imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => ({
            secret: configService.get<string>('JWT_SECRET'),
            signOptions: {
                expiresIn: configService.get<string | number>('JWT_EXPIRES')
            },
        }),
        }),
    AuthModule,
    MongooseModule.forFeature([{ name: CarPost.name, schema: CarPostSchema }, { name: User.name, schema: UserSchema }, { name: Comment.name, schema: CommentSchema },  { name: Notification.name, schema: NotificationSchema }] ),
],
    providers: [CommentService, NotificationService],
    controllers: [CommentController],
})
export class CommentModule {}
