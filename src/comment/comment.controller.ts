/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { Request} from 'express';
import { CreateCommentDto } from './dto/createComment.dto';
import { AuthGuard } from '@nestjs/passport';
import { UpdateCommentDto } from './dto/updateComment.dto';

@Controller('comments')
export class CommentController {
    constructor (private readonly commentService: CommentService){}
    
    @UseGuards(AuthGuard("jwt"))
    @Post("create")
    create(@Req() req, @Body() createCommentDto : CreateCommentDto){
        const userId= req.user;
        return this.commentService.create(userId, createCommentDto)
    }

    @UseGuards(AuthGuard("jwt"))
    @Delete("delete/:id")
    async delete(@Req() req, @Param('id') commentId:string) {
        try {
            const user = req.user;
            const postId = req.body.postId;
            return await this.commentService.delete(commentId, user, postId);
        } catch (error) {
            throw error;
        }
    }

    @UseGuards(AuthGuard('jwt'))
    @Put("update/:id")
    async update (@Param('id') commentId:string, @Req() req, @Body() updateCommentDto:UpdateCommentDto){
        try {
            const user = req.user;
            return await this.commentService.update(commentId, user, updateCommentDto);
        } catch (error) {
            throw error;
        }
    }

    @Get()
    getComments() {
        return this.commentService.getComments();
    }

    @Get(":postId")
    getCommentsByPostId(@Param('postId') postId: string) {
    return this.commentService.getCommentsByPostId(postId);
}



    @UseGuards(AuthGuard("jwt"))
    @Post(":id/favorite")
    async favorite(@Req() req, @Param('id') commentId:string) {
        try {
            const userId = req.user;
            return await this.commentService.favorite(commentId, userId);
        } catch (error) {
            throw error;
        }
    }

    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/unfavorite")
    async unfavorite(@Req() req, @Param('id') commentId:string) {
        try {
            const userId = req.user;
            return await this.commentService.unfavorite(commentId, userId);
        } catch (error) {
            throw error;
        }
    }

}
