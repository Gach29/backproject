/* eslint-disable prettier/prettier */

import { IsNotEmpty, IsOptional, IsString } from "class-validator";


export class CreateCommentDto {
    @IsNotEmpty()
    readonly content:string;

    @IsNotEmpty()
    readonly postId:string ;


    @IsOptional() 
    @IsString()
    @IsNotEmpty()
    parentCommentId?: string;


}
