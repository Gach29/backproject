/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { CreateCommentDto } from './dto/createComment.dto';
import { InjectModel } from '@nestjs/mongoose';
import { CarPost } from 'src/schemas/carpost.schema';
import { Model } from 'mongoose';
import { Comment } from 'src/schemas/comments.schema';
import { UpdateCommentDto } from './dto/updateComment.dto';
import { User } from 'src/schemas/user.schema';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { CommentCreatedEvent } from 'src/events/comment-created.event';
import { NotificationService } from 'src/notification/notification.service';
import { Notification } from 'src/schemas/notifications.schema';
import { Types } from 'mongoose';


@Injectable()
export class CommentService {

    constructor(
        @InjectModel(CarPost.name) private readonly carPostModel: Model<CarPost>,
        @InjectModel(User.name) private readonly userModel: Model<User>,
        @InjectModel(Comment.name) private readonly commentModel: Model<Comment>,
        @InjectModel(Notification.name) private readonly notificationModel: Model<Notification>,
        private readonly notificationService: NotificationService,
        private readonly eventEmitter : EventEmitter2,
    ) {}

    // async create(userId: string, createCommentDto: CreateCommentDto) {
    //     const { postId, content } = createCommentDto;
    
    //     // Vérifier si l'utilisateur existe
    //     const user = await this.userModel.findById(userId);
    //     if (!user) {
    //         throw new NotFoundException('User not found');
    //     }
    
    //     try {
    //         const newComment = new this.commentModel({
    //             content,
    //             user: userId, 
    //             post: postId,
    //         });
    
    //         const comment = await newComment.save();
    
    //         const post = await this.carPostModel.findById(postId);
    //         if (!post) {
    //             throw new NotFoundException('Post not found');
    //         }
    
    //         // Ajouter le commentaire à la liste des commentaires du post
    //         const populatedComment = await this.commentModel.populate(comment, { path: 'user', select: 'name email content' });
    //         post.comments.push(populatedComment);
    
    //         // Ajouter le commentaire à la liste des commentaires de l'utilisateur
    //         user.comments.push(populatedComment);
    
    //         // Enregistrer les modifications
    //         await post.save();
    //         await user.save();
    
    //         return { message: "Comment created!", postId, data: comment };
    //     } catch (error) {
    //         throw new Error('Failed to save the comment to the post');
    //     }
    // }

    // async create(userId: string, createCommentDto: CreateCommentDto) {
    //     const { postId, content, parentCommentId } = createCommentDto;
    
    //     const user = await this.userModel.findById(userId);
    //     if (!user) {
    //         throw new NotFoundException('User not found');
    //     }
    
    //     try {
    //         const newComment = new this.commentModel({
    //             content,
    //             user: userId,
    //             post: postId,
    //             parentComment: parentCommentId, 
    //         });
    
    //         const comment = await newComment.save();
    
    //         const post = await this.carPostModel.findById(postId);
    //         if (!post) {
    //             throw new NotFoundException('Post not found');
    //         }
    
    //         const populatedComment = await this.commentModel.populate(comment, { path: 'user', select: 'name email profileImage content' });
    //         post.comments.push(populatedComment);
    
    //         user.comments.push(populatedComment);
    
    //         await post.save();
    //         await user.save();
    
    //         if (parentCommentId) {
    //             const parentComment = await this.commentModel.findById(parentCommentId);
    //             if (!parentComment) {
    //                 throw new NotFoundException('Parent comment not found');
    //             }
    
    //             parentComment.replies.push(populatedComment);
    //             await parentComment.save();
    //         }
            
    //         this.eventEmitter.emit('comment.created',new CommentCreatedEvent(populatedComment, user, post))
            
    //         return { message: "Comment created!", postId, data: comment };
    //     } catch (error) {
    //         throw new Error('Failed to save the comment to the post');
    //     }
    // }

    // @OnEvent('comment.created')
    // async handleCommentCreated(event: CommentCreatedEvent) {
    //     const { comment, user, post } = event;

    //     try {
    //         const postUser = await this.userModel.findById(post.user);
    //         if (!postUser) {
    //             throw new NotFoundException('User who created the post not found');
    //         }

    //         if (!Types.ObjectId.isValid(post._id)) {
    //             throw new Error('Invalid ObjectId for user or post');
    //         }

    //         const notificationContent = `${user.name} a commenté votre post "${post.marque} ${post.modele}"`;

    //         const newNotification = new this.notificationModel({
    //             userId: post.user.name,
    //             content: notificationContent,
    //             postId: post._id,
    //             read: false
    //         });

    //         postUser.notifications.push(newNotification);
    //         await postUser.save();
    //         console.log('Notification added to user:', newNotification);
    //     } catch (error) {
    //         console.error('Error handling comment created event:', error);
    //     }
    // }

    async create(userId: string, createCommentDto: CreateCommentDto) {
        const { postId, content, parentCommentId } = createCommentDto;
    
        const user = await this.userModel.findById(userId);
        if (!user) {
            throw new NotFoundException('User not found');
        }
    
        try {
            const newComment = new this.commentModel({
                content,
                user: userId,
                post: postId,
                parentComment: parentCommentId, 
            });
    
            const comment = await newComment.save();
    
            const post = await this.carPostModel.findById(postId);
            if (!post) {
                throw new NotFoundException('Post not found');
            }
    
            const populatedComment = await this.commentModel.populate(comment, { path: 'user', select: 'name email profileImage content' });
            post.comments.push(populatedComment);
    
            user.comments.push(populatedComment);
    
            await post.save();
            await user.save();
    
            if (parentCommentId) {
                const parentComment = await this.commentModel.findById(parentCommentId);
                if (!parentComment) {
                    throw new NotFoundException('Parent comment not found');
                }
    
                parentComment.replies.push(populatedComment);
                await parentComment.save();
            }
            
            this.eventEmitter.emit('comment.created', new CommentCreatedEvent(populatedComment, user, post));

            const postUser = await this.userModel.findById(post.user);
            if (!postUser) {
                throw new NotFoundException('User who created the post not found');
            }

            const notificationContent = `${user.name} a commenté votre post "${post.marque} ${post.modele}"`;

            const newNotification =await this.notificationService.createNotification(post.user.name, post.user.toString(), notificationContent,post._id);


            postUser.notifications.push(newNotification);
            await postUser.save();

            return { message: "Comment created!", postId, data: comment };
        } catch (error) {
            throw new Error('Failed to save the comment to the post');
        }
    }


    
    

    async delete(commentId: string, userId: string, postId: string) {
        try {
            if (!userId) {
                throw new UnauthorizedException('User is not authenticated');
            }
    
            const comment = await this.commentModel.findById(commentId);
            if (!comment) {
                throw new NotFoundException('Comment not found');
            }
    
            if (comment.post.toString() !== postId) {
                throw new UnauthorizedException('PostId does not match');
            }
    
            const post = await this.carPostModel.findById(postId);
            if (!post) {
                throw new NotFoundException('Post not found');
            }
    
            // if (!post.comments) {
            //     post.comments = [];
            // }
    
            post.comments = post.comments.filter(comment => comment._id.toString() !== commentId);
    
            await post.save();
    
            await this.commentModel.deleteOne({ _id: commentId });
    
            return { message: "Comment deleted" };
        } catch (error) {
            throw error; 
        }
    }

    async getComments() {
        try {
            const comments = await this.commentModel
            .find()
            .populate('user', 'name  profileImage') 
            .populate({
                path: 'replies',
                populate: [
                    { path: 'user', select: 'name profileImage' },
                    { path: 'favorites',select: 'name profileImage' }
                ]
            })
            .populate({
                path: 'favorites',
                select: 'name profileImage' 
            })
            .select('content user createdAt replies favorites') 
            .exec();
        return comments;
        } catch (error) {
            throw new NotFoundException('Comments not found');
        }
    }
    
    
    async update(commentId: string, userId: string, updateCommentDto: UpdateCommentDto) {
        const { content, postId } = updateCommentDto;
        try {
            if (!userId) {
                throw new UnauthorizedException('User is not authenticated');
            }
    
            const comment = await this.commentModel.findById(commentId);
            if (!comment) {
                throw new NotFoundException('Comment not found');
            }
    
            if (comment.post.toString() !== postId) {
                throw new UnauthorizedException('PostId does not match');
            }
    
            await this.commentModel.findByIdAndUpdate(commentId, { content });

            await comment.save();
            
            return { message: "Comment updated" };
        } catch (error) {
            throw error;
        }
    }


    async favorite(commentId: string, userId: string) {
        try {
            const comment = await this.commentModel.findById(commentId);
            if (!comment) {
                throw new NotFoundException('Comment not found');
            }

            const post = await this.carPostModel.findById(comment.post);
            if (!post) {
                throw new NotFoundException('Post not found');
            }

            const user = await this.userModel.findById(userId);
            if (!user) {
                throw new NotFoundException('Comment not found');
            }

            const commentUser = await this.userModel.findById(comment.user);
            if (!user) {
                throw new NotFoundException('Comment not found');
            }
    
            if (!comment.favorites.includes(userId)) {
                comment.favorites.push(userId);
                await comment.save();
            }

    
            const notificationContent = `${user.name} a aimé votre commentaire "${comment.content}" sur le post de "${post.marque} ${post.modele}"`;
    
            const newNotification =await this.notificationService.createNotification(comment._id, comment.user.toString(), notificationContent, post._id);
            
            commentUser.notifications.push(newNotification);
            await commentUser.save();

            return { message: "Comment favorited" };
        } catch (error) {
            throw error;
        }
    }
    
    
    async unfavorite(commentId: string, userId: string) {
        try {
            const comment = await this.commentModel.findById(commentId);
            if (!comment) {
                throw new NotFoundException('Comment not found');
            }
            
            comment.favorites = comment.favorites.filter(fav => fav.toString() !== userId);
            await comment.save();
            
            return { message: "Comment unfavorited" };
        } catch (error) {
            throw error;
        }
    }
    
    async getCommentsByPostId(postId: string) {
        try {
            const comments = await this.commentModel
                .find({ post: postId })
                .populate('user', 'name profileImage')
                .populate({
                    path: 'replies',
                    populate: [
                        { path: 'user', select: 'name profileImage' },
                        { path: 'favorites', select: 'name profileImage' }
                    ]
                })
                .populate({
                    path: 'favorites',
                    select: 'name profileImage'
                })
                .select('content user createdAt replies favorites')
                .exec();
            return comments;
        } catch (error) {
            throw new NotFoundException('Comments not found for the specified post ID');
        }
    }
    
    
}


    
    