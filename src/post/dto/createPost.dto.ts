/* eslint-disable prettier/prettier */
import { IsArray, IsEmpty, IsNotEmpty,IsOptional, IsString } from 'class-validator';
import { User } from 'src/schemas/user.schema';

export class CreateCarPostDto {


    @IsNotEmpty({ message: "Marque est obligatoire" })
    @IsString()
    marque: string;

    @IsNotEmpty({ message: "Modèle est obligatoire" })
    @IsString()
    modele: string;

    @IsNotEmpty({ message: "Année est obligatoire" })
    annee: number;

    @IsNotEmpty({ message: "Age est obligatoire" })
    age: number;

    @IsNotEmpty({ message: "Kilométrage est obligatoire" })
    kilometrage: number;

    @IsNotEmpty({ message: "Énergie est obligatoire" })
    @IsString()
    energie: string;

    // @IsNotEmpty({ message: "Boite de vitesse est obligatoire" })
    @IsString()
    boiteDeVitesse: string;

    @IsNotEmpty({ message: "Carrosserie est obligatoire" })
    @IsString()
    carrosserie: string;

    @IsNotEmpty({ message: "Transmission est obligatoire" })
    @IsString()
    transmission: string;

    @IsNotEmpty({ message: "Puissance est obligatoire" })
    @IsString()
    puissance: string;

    @IsNotEmpty({ message: "Nombre de portes est obligatoire" })
    nombrePortes: number;

    @IsNotEmpty({ message: "Couleur extérieure est obligatoire" })
    @IsString()
    couleurExterieure: string;

    @IsNotEmpty({ message: "Couleur intérieure est obligatoire" })
    @IsString()
    couleurInterieure: string;

    @IsNotEmpty({ message: "Sellerie est obligatoire" })
    @IsString()
    sellerie: string;

    @IsArray()
    @IsOptional()
    files: string[];

    @IsOptional()
    prix: number;

    @IsArray()
    @IsOptional()
    selectedOptions: string[]; 

    @IsOptional()
    location: string;

    // @IsNotEmpty()
    // @IsString()
    // contact: string;

    // @IsArray()
    // features: string[];
    

    @IsEmpty({ message : "Vous ne pouvez pas ecrire l'id"})
    readonly user : User


}   