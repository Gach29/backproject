/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CarPost, CarPostSchema } from 'src/schemas/carpost.schema';
import { User, UserSchema } from 'src/schemas/user.schema';
import { JwtModule } from '@nestjs/jwt'; // Importez JwtModule
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from 'src/auth/auth.module';
import {Notification, NotificationSchema } from 'src/schemas/notifications.schema';
import { NotificationService } from 'src/notification/notification.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: {
          expiresIn: configService.get<string | number>('JWT_EXPIRES')
        },
      }),
    }),
    AuthModule,
    MongooseModule.forFeature([{ name: CarPost.name, schema: CarPostSchema }, { name: User.name, schema: UserSchema }, { name: Notification.name, schema: NotificationSchema }]),
  ],
  providers: [PostService,NotificationService],
  controllers: [PostController],
})
export class PostModule {}
