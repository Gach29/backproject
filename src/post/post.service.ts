/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
//import { CreateCarPostDto } from './dto/createPost.dto';
import { CarPost } from 'src/schemas/carpost.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

// import { MailerService } from 'src/mailer/mailer.service';
import { User } from 'src/schemas/user.schema';
import { CreateCarPostDto } from './dto/createPost.dto';
import { extname,posix,parse } from 'path';
import { diskStorage } from 'multer';
import * as path from 'path';
import * as fs from 'fs';
import { promisify } from 'util';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { FavorisAddedEvent } from 'src/events/favoris-added.event';
import { Notification } from 'src/schemas/notifications.schema';
import { NotificationService } from 'src/notification/notification.service';

const asyncWriteFile = promisify(fs.writeFile);


export const storage = {
    storage: diskStorage({
        destination: './uploads/carPhotos',
        filename: (req, file, cb) => {
            const originalFileName = path.parse(file.originalname).name;
            const uniqueFileName = `${originalFileName}-${Date.now()}-${Math.round(Math.random() * 1E9)}${path.extname(file.originalname)}`;
            cb(null, uniqueFileName);
        },
    }),
    fileFilter: (req, file, cb) => {
        const ext = extname(file.originalname).toLowerCase();
        if (ext === '.jpg' || ext === '.jpeg' || ext === '.png') {
            cb(null, true);
        } else {
            cb(new BadRequestException('Seuls les fichiers de type PDF, Word, JPG ou PNG sont autorisés'), false);
        }
    }
}
@Injectable()
export class PostService {
    constructor(
        @InjectModel(CarPost.name) private readonly carPostModel: Model<CarPost>,
        @InjectModel(User.name) private readonly userModel: Model<User>,
        @InjectModel(Notification.name) private readonly notificationModel: Model<Notification>,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
        private readonly notificationService: NotificationService,
        private readonly eventEmitter : EventEmitter2,

    ) { }



    async createPost(createCarPostDto: CreateCarPostDto, userId: string) {
        try {
            const { marque, modele, annee, age, kilometrage, energie, boiteDeVitesse, carrosserie, transmission, puissance, nombrePortes, files,location, couleurExterieure, couleurInterieure, sellerie, prix, selectedOptions } = createCarPostDto;

        const user = await this.userModel.findById(userId);
        if (!user) {
            throw new Error('User not found');
        }

    

            // let fileNames = [];
            // fileNames = await Promise.all(files.map(async (file: any) => {
            //     const fileName = `${file.originalname}`;
            //     const filePath = path.join('uploads', 'carPhotos', fileName);
            //     await asyncWriteFile(filePath, file.buffer);
            //     return fileName;
            // }))


            const newPost = new this.carPostModel({
                marque,
                modele,
                annee,
                age,
                kilometrage,
                energie,
                boiteDeVitesse,
                carrosserie,
                transmission,
                puissance,
                nombrePortes,
                couleurExterieure,
                couleurInterieure,
                sellerie,
                prix,
                selectedOptions,
                location,
                files,
                user: user._id
            });


            const savedPost = await newPost.save();
            user.posts.push(savedPost);
            await user.save();
            return { token: '', data: 'Post créé avec succès', newPost: savedPost };
        
        } catch (error) {
            console.log(error);
            
        }
        
    }



    async uploadCarPhoto(files: Express.Multer.File[]) {
        try {
            const baseUrl = 'http://localhost:8000'; 
            const filePaths = [];
            for (const file of files) {
                const originalFileName = path.parse(file.originalname).name; 
                const uniqueFileName = `${originalFileName}${path.extname(file.originalname)}`;
                const fileName = uniqueFileName;
                const filePath = posix.join('uploads', 'carPhotos', fileName); 
                // const fileUrl = posix.join(baseUrl, filePath); 
                await asyncWriteFile(filePath, file.buffer); 
                filePaths.push(fileName);
            }
            // let fileNames = [];
            // fileNames = await Promise.all(files.map(async (file: any) => {
            //     const fileName = `${file.originalname}`;
            //     const filePath = path.join('uploads', 'carPhotos', fileName);
            //     await asyncWriteFile(filePath, file.buffer);
            //     fileNames.push(fileName)
            // }))
    
            return { success: true, message: "Fichiers téléchargés avec succès", filePaths };
        } catch (error) {
            return { success: false, message: "Une erreur est survenue lors du téléchargement des fichiers", error };
        }
    }





    async getPosts(): Promise<CarPost[]> {
        try {
            const posts = await this.carPostModel.find()
                .populate({
                    path: 'comments',
                    populate: {
                        path: 'user',
                        select: '_id name profileImage'
                    },
                    select: 'content _id'
                })
                .populate('user')
                .exec();
            return posts;
        } catch (error) {
            console.error('Error fetching posts:', error);
            throw new Error('Failed to fetch posts');
        }
    }



    async getPostById(id: string): Promise<CarPost> {
        try {
            if (!id) {
                throw new Error('ID parameter is empty or null');
            }
    
            const post = await this.carPostModel.findById(id).exec();
            if (!post) {
                throw new Error('Post not found');
            }
            return post;
        } catch (error) {
            console.error('Error fetching post by ID:', error);
            throw new Error('Failed to fetch post by ID');
        }
    }
    
    

    async getUserPosts(userId: string): Promise<CarPost[]> {
        try {
            const posts = await this.carPostModel.find({ user: userId }).exec();
            return posts;
        } catch (error) {
            console.error('Error fetching user posts:', error);
            throw new Error('Failed to fetch user posts');
        }
    }
    


    async postsFavoris(postId: string, userId: string) {
        try {
            const user = await this.userModel.findById(userId);
            const post = await this.carPostModel.findById(postId);
        
            if (!user || !post) {
                throw new Error('Utilisateur ou post introuvable.');
            }
            
            const postUser = await this.userModel.findById(post.user);
    
            const isPostInFavoris = user.favoris.some(favori => favori.toString() === post._id.toString());
        
            if (isPostInFavoris) {
                return { data: 'Post déjà ajouté aux favoris.' };
            }
        
            user.favoris.push(post._id);
            post.favoris.push(user);
        
            await user.save();
            await post.save();
    
            this.eventEmitter.emit('favoris.added', new FavorisAddedEvent(user, post));
    
            const notificationContent = `${user.name} a aimé votre post "${post.marque} ${post.modele}"`;

            const newNotification = await this.notificationService.createNotification(post.user.name, post.user.toString(), notificationContent, post._id);
    
            postUser.notifications.push(newNotification);
            await postUser.save();
    
            return { data: 'Post ajouté aux favoris.' };
        } catch (error) {
            throw error;
        }
    }
    

    async Search(key: string) {
        const isInteger = /^\d+$/.test(key);
        const searchCriteria = [];

        if (!isInteger) {
            searchCriteria.push(
                { make: { $regex: key, $options: 'i' } },
                { location: { $regex: key, $options: 'i' } },
                { carModel: { $regex: key, $options: 'i' } }
            );
        }

        if (isInteger) {
            const numericKey = parseInt(key);
            searchCriteria.push(
                { price: numericKey },
                { year: numericKey },
                { mileage: numericKey }
            );
        }

        const keyword = searchCriteria.length > 0 ? { $or: searchCriteria } : {};

        const posts = await this.carPostModel.find(keyword);

        return posts;
    }



}