/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { BadRequestException, Body, Controller, Get, Param, Post, Query, Req, Res, UnauthorizedException, UploadedFile, UploadedFiles, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { PostService } from './post.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateCarPostDto } from './dto/createPost.dto';
import { JwtService } from '@nestjs/jwt';
import {Query as QuerySearch} from 'express-serve-static-core'
import { CarPost } from 'src/schemas/carpost.schema';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path = require('path');
import { extname,posix,join,parse } from 'path';
import { Observable, of } from 'rxjs';
import * as fs from 'fs';



@Controller('posts')
export class PostController {
    constructor(
        private readonly postService: PostService,
        private readonly jwtService: JwtService,
    ) {}
    

    

    @UseGuards(AuthGuard('jwt'))
    @Post('/create')
    async createPost(@Body() createCarPostDto: CreateCarPostDto, @Req() req) {
        const authorizationHeader = req.headers['authorization'];
        if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
            throw new UnauthorizedException('Token is missing in Authorization header');
        }
        const userId = req.user.id;

        return this.postService.createPost(createCarPostDto,userId);
        
    }

    @Post('/uploadcarphoto')
    @UseInterceptors(FilesInterceptor('files', 10, { 
        storage: diskStorage({
            destination: './uploads/carPhotos',
            filename: (req, file, cb) => {
                const originalFileName = path.parse(file.originalname).name; 
                const uniqueFileName = `${originalFileName}${path.extname(file.originalname)}`;
                cb(null, uniqueFileName);
            },
        }),
        fileFilter: (req, file, cb) => {
            const ext = extname(file.originalname).toLowerCase();
            if (ext === '.jpg' || ext === '.jpeg' || ext === '.png') {
                cb(null, true);
            } else {
                cb(new BadRequestException('Seuls les fichiers de type JPG ou PNG sont autorisés'), false);
            }
        }
    }))
    async uploadCarPhoto(@UploadedFiles() files: Express.Multer.File[]) {
        try {
            const baseUrl = 'http://localhost:8000'; 
            const filePaths = [];
            for (const file of files) {
                const originalFileName = path.parse(file.originalname).name; 
                const uniqueFileName = `${originalFileName}${path.extname(file.originalname)}`;
                const fileName = uniqueFileName;
                const filePath = posix.join('uploads', 'carPhotos', fileName);
                // const fileUrl = posix.join(baseUrl, filePath); 
                filePaths.push(fileName);
            }
    
            return { success: true, message: "Fichiers téléchargés avec succès", filePaths };
        } catch (error) {
            return { success: false, message: "Une erreur est survenue lors du téléchargement des fichiers", error };
        }
    }

    @Get('carPhotos')
    async getCarPhotosList() {
        try {
            const files = await fs.promises.readdir('./uploads/carPhotos');
            return files;
        } catch (error) {
            console.error('Error fetching car photos:', error);
            throw new Error('Failed to fetch car photos');
        }
    }
    
    @Get('carPhotos/:filename')
    getPicture(@Param('filename') filename: string, @Res() res) {
        return res.sendFile(filename, { root: './uploads/carPhotos' });
    }
    
    @Get()
    getPosts() {
        return this.postService.getPosts(); 
    }

    @Get('/post/:id')
    getPostById(@Param('id') id: string) {
    return this.postService.getPostById(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('/user-posts/:userId')
    async getUserPostsById(@Param('userId') userId: string) {
        return this.postService.getUserPosts(userId);
    }

    

    @UseGuards(AuthGuard('jwt'))
    @Post('favoris/:id')
    async postsFavoris(@Param('id') postId: string,@Req() req) 
    {
        const userId=req.user._id;
        return this.postService.postsFavoris(postId, userId);
    }



    @Post('/search')
    Search(@Query('key') key){
        return this.postService.Search(key);
    }


}
