/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from './user.schema';
import { Comment } from './comments.schema';

@Schema({ timestamps: true })
export class CarPost extends Document {

    @Prop({ required: true })
    marque: string;

    @Prop({ required: true })
    modele: string;

    @Prop({ required: true })
    annee: number;

    @Prop({ required: true })
    age: number;

    @Prop({ required: true })
    energie: string;

    @Prop()
    boiteDeVitesse: string;

    @Prop({ required: true })
    carrosserie: string;

    @Prop({ required: true })
    transmission: string;

    @Prop({ required: true })
    puissance: string;

    @Prop({ required: true })
    kilometrage: number;

    @Prop({ required: true })
    nombrePortes: number;

    @Prop({ required: true })
    couleurExterieure: string;

    @Prop({ required: true })
    couleurInterieure: string;

    @Prop({ required: true })
    sellerie: string;

    @Prop({ required: true  })
    files: string[]; 

    @Prop()
    prix: number;

    @Prop()
    selectedOptions: string[];


    @Prop()
    location: string;

    // @Prop()
    // features: string[];

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
    user: User;


    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }] }) 
    comments: Comment[];

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
    favoris: User[];



    }

export const CarPostSchema = SchemaFactory.createForClass(CarPost);