/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { CarPost } from './carpost.schema';
import { Comment } from './comments.schema';
import { Notification } from './notifications.schema';

export interface Demandes {
    user: string;
    post: string;
}

@Schema({
    timestamps: true
})
export class User {

    @Prop()
    name: string;

    @Prop({ unique: [true, 'Email existe déja'] })
    email: string;

    @Prop()
    password: string;

    @Prop()
    phone: string;

    @Prop({ default: null })
    address: string | null;

    @Prop({ default: null })
    profileImage: string | null;

    @Prop({ default: null })
    expertCertif: string | null;

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }] })
    comments: Comment[];

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost' }] })
    posts: CarPost[];

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost' })
    post: CarPost;

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost' }] })
    favoris: CarPost[]

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Notification' }] })
    notifications: Notification[]

    @Prop({ default: 'user' }) 
    role: string;

    @Prop({ default: [] })
    demandes: Demandes[];




}

export const UserSchema = SchemaFactory.createForClass(User);
