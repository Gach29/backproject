/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

// // Définir la structure d'un élément de ligne
// export interface LineItem {
//     // Propriétés de l'élément de ligne
//     // Par exemple : 
//     name: string;
//     quantity: number;
//     // Ajoutez d'autres propriétés selon les besoins
// }

// // Définir la structure de la session de paiement
// export type CheckoutSessionDocument = CheckoutSession & Document;

// // @Schema({ timestamps: true })
// // export class CheckoutSession extends Document {
// //     @Prop({ required: true })
// //     sessionId: string;

// //     @Prop({ required: true })
// //     priceId: string;

// //     @Prop({ required: true })
// //     url: string;

// //     @Prop({ type: [Object], default: [] }) // Tableau d'objets représentant les éléments de ligne
// //     lineItems: LineItem[]; // Propriété pour stocker les éléments de ligne
// // }

// // export const CheckoutSessionSchema = SchemaFactory.createForClass(CheckoutSession);


@Schema({ timestamps: true })
export class Payment {

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User',required:true })
    userId: string;

    @Prop({ required: true })
    amount:string

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost' })
    postId: string; 

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost' })
    postUser: string; 

}

export const PaymentSchema = SchemaFactory.createForClass(Payment);
