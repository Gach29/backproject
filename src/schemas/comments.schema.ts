/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from './user.schema';
import { CarPost } from './carpost.schema';

export type  CommentDocument = Comment & Document;

@Schema({ timestamps: true })

export class Comment extends Document {
    @Prop({ required: true })
    content: string;
    

    
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User',required:true })
    user: User;

    createdAt: Date;


    // @Prop({ type: String, ref: 'User' }) // Référence à l'utilisateur qui a créé le commentaire
    // userId: string;


    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost'})
    post: CarPost;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CarPost' })
    postId: string; 

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }], default: [] })
    replies: Comment[];

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }], default: [] })
    favorites: string[]; 


}


export const CommentSchema = SchemaFactory.createForClass(Comment);