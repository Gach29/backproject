/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
// import { CarPost } from './carpost.schema';
// import mongoose from 'mongoose';

export interface Demandes {
    user: string;
    post: string;
}

@Schema({ timestamps: true})
export class Expert {
    
    @Prop()
    name: string;

    @Prop({unique : [true, 'Email existe déja'] })
    email : string;

    @Prop()
    phone: string;

    @Prop()
    address: string;

    @Prop()
    certif: string;

    @Prop({ default: 'expert' }) 
    role: string;

    @Prop({ default: [] })
    demandes: Demandes[];

    @Prop({ default: [] })
    demandesAcceptees: Demandes[];

    @Prop({ default: [] })
    demandesRefusees: Demandes[];

    @Prop()
    accepted: boolean;

    

    // @Prop({ default: [] })
    // usersDemandes: string[];




}

export const ExpertSchema = SchemaFactory.createForClass(Expert);