/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { User } from './user.schema';
import { CarPost } from './carpost.schema';

export type NotificationDocument = Notification & Document;

@Schema({ timestamps: true })
export class Notification {
    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'User' })
    user: User;

    @Prop()
    content: string;

    @Prop({ default: false })
    read: boolean;

    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'CarPost' })
    postId: CarPost;

    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'User' }) 
    userId: MongooseSchema.Types.ObjectId;


    @Prop({ type: MongooseSchema.Types.String }) 
    commentContent: string;

}

export const NotificationSchema = SchemaFactory.createForClass(Notification);
