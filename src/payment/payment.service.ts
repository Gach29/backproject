/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import {HttpService } from '@nestjs/axios';
import Stripe from 'stripe';
// import { Cart } from './Cart.model';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { MailerService } from 'src/mailer/mailer.service';
import { InjectModel } from '@nestjs/mongoose';
import { CarPost } from 'src/schemas/carpost.schema';
import { User } from 'src/schemas/user.schema';
import { Model } from 'mongoose';
import { Observable, catchError } from 'rxjs';
import { Payment } from 'src/schemas/payment.schema';
import { use } from 'passport';


@Injectable()
export class PaymentService {
    // private stripe;

    constructor(
        @InjectModel(CarPost.name) private readonly carPostModel: Model<CarPost>,
        @InjectModel(User.name) private readonly userModel: Model<User>,
        @InjectModel(Payment.name) private readonly paymentModel: Model<Payment>,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
        private readonly emailService : MailerService,
        private httpService: HttpService
    ){
        // this.stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {apiVersion:'2024-04-10'})
    }

    async add(amount: string, userId: string, postId: string): Promise<any> {
        const url = 'https://developers.flouci.com/api/generate_payment';
        const payload = {
            app_token: 'b71e6bc6-e307-4f99-8167-7802a2ce8964',
            app_secret: process.env.FLOUCI_SECRET,
            amount: amount,
            accept_card: 'true',
            session_timeout_secs: 1200,
            success_link: 'http://localhost:3000/successpayment',
            fail_link: 'http://localhost:3000/failpayment',
            developer_tracking_id: '52d7d8b2-dba8-42f6-8a8f-d0148aca5433',
            userId : userId,
            postId : postId
        };

        const post = await this.carPostModel.findById(postId);
        if (!post) {
            throw new NotFoundException('Post non trouvé');
        }
        const postUser = await this.userModel.findById(post.user);
        if (!postUser) {
            throw new NotFoundException('User de post non trouvé');
        }
    
        try {
            const checkoutSession = new this.paymentModel({
                userId: userId, 
                amount: amount,
                postId: postId,
                paymentReceiver:postUser
            });
            await checkoutSession.save();
    
            const response = await this.httpService.post(url, payload).toPromise();
            const responseData = response.data;
    
            if (responseData.result.success) {
                const post = await this.carPostModel.findById(postId);
                if (!post) {
                    throw new NotFoundException('Post non trouvé');
                }

                const postUser = await this.userModel.findById(post.user);
                if (!postUser) {
                    throw new NotFoundException('User de post non trouvé');
                }
    
                const user = await this.userModel.findById(userId);
                if (!user) {
                    throw new NotFoundException('User non trouvé');
                }
                
                const car=post.marque + '' + post.modele
                await this.emailService.PaymentSuccess(user.email, amount, car);
                console.log("Email envoyé avec succès");

                const email=postUser.email;
                console.log(email)
                await this.emailService.PaymentSent(email, user.name, post.marque);
                console.log("Email envoyé avec succès");
            }
    
            return responseData;
    
        } catch (error) {
            console.error('Failed to add payment:', error);
        }
    }
    

    async verify(payment_id: string): Promise<any> {
        const url = `https://developers.flouci.com/api/verify_payment/${payment_id}`;
        const headers = {
            'Content-Type': 'application/json',
            'apppublic': 'b71e6bc6-e307-4f99-8167-7802a2ce8964',
            'appsecret': process.env.FLOUCI_SECRET
        };
    

        try {
            const response = await this.httpService.get(url, { headers }).toPromise();
            const responseData = response.data;

            if (responseData.status === 'SUCCESS') {
                console.log('Success')
            }

            return responseData;
        } catch (error) {
            throw new Error('Failed to verify payment');
        }
    }

    async findByPostId(postId: string): Promise<Payment[]> {
        return await this.paymentModel.find({ postId }).exec();
    }

    async findByUserId(userId: string): Promise<Payment[]> {
        return await this.paymentModel.find({ userId }).exec();
    }


    // async verify(payment_id: string): Promise<any> {
    //     const url = `https://developers.flouci.com/api/verify_payment/${payment_id}`;
    //     const headers = {
    //         'Content-Type': 'application/json',
    //         'apppublic': 'b71e6bc6-e307-4f99-8167-7802a2ce8964',
    //         'appsecret': process.env.FLOUCI_SECRET
    //     };

    //     return this.httpService.get(url, { headers }).pipe(
    //         catchError(error => {
    //             throw new Error(error.message);
    //         })
    //     );
    // }

    // async add(amount: string): Promise<any> {
    //     const url = 'https://developers.flouci.com/api/generate_payment';
    //     const payload = {
    //         app_token: 'b71e6bc6-e307-4f99-8167-7802a2ce8964',
    //         app_secret: process.env.FLOUCI_SECRET,
    //         amount: amount,
    //         accept_card: 'true',
    //         session_timeout_secs: 1200,
    //         success_link: 'http://localhost:3000/successpayment',
    //         fail_link: 'http://localhost:3000/failpayment',
    //         developer_tracking_id: '52d7d8b2-dba8-42f6-8a8f-d0148aca5433',
    //     };
    
    //     try {
    //         const response = await this.httpService.post(url, payload).toPromise();
    //         return response.data;
    //     } catch (error) {
    //         throw new Error('Failed to add payment');
    //     }
    // }

    
}

//  async checkout(amount: number ,userId: string,postId:string) {
//         try {
//             const post = await this.carPostModel.findById(postId);
//             if (!post) {
//                 throw new Error('User not found');
//             }
//             const user = await this.userModel.findById(userId);
//             if (!user) {
//                 throw new Error('User not found');
//             }

//             if (!amount || amount <= 0) {
//                 throw new Error('Invalid amount');
//             }
    
//             const paymentIntent = await this.stripe.paymentIntents.create({
//                 amount: amount,
//                 currency: 'usd',
//                 payment_method_types: ['card'],
//                 metadata: {
//                     orderId: '123456',
//                     customerName: 'Chaima',
//                     createdAt: new Date().toISOString(),
//                 },
//             });
    
//             const confirmedPaymentIntent = await this.stripe.paymentIntents.confirm(paymentIntent.id, {
//                 payment_method: 'pm_card_visa',
//             });

//             await this.emailService.PaymentSuccess(user.email, post.user.toString(), post.marque);
    
//             return { payment: confirmedPaymentIntent, clientSecret: confirmedPaymentIntent.client_secret };
//         } catch (error) {
//             return { error: error.message };
//         }
//     }
    

//     // async checkout(cart: Cart) {
//     //     const totalPrice = cart.reduce((acc, item) => acc + item.quantity * item.price, 0);
    
//     //     const paymentIntent = await this.stripe.paymentIntents.create({
//     //         amount: totalPrice,
//     //         currency: 'usd',
//     //         payment_method_types: ['card'], 
//     //         metadata: {
//     //             orderId: '123456',
//     //             customerName: 'Chaima',
//     //             createdAt: new Date().toISOString(),
//     //         },
//     //     });
        
//     //     const confirmedPaymentIntent = await this.stripe.paymentIntents.confirm(paymentIntent.id, {
//     //         payment_method: 'pm_card_visa', 
//     //     });
    
//     //     return { payment: confirmedPaymentIntent, clientSecret: confirmedPaymentIntent.client_secret };   
//     // }

//     async getClientSecret() {
//         const paymentIntent = await this.stripe.paymentIntents.create({
//             amount: 1000,
//             currency: 'usd',
//             payment_method_types: ['card'], 
//             metadata: {
//                 orderId: '123456',
//                 customerName: 'Chaima',
//                 createdAt: new Date().toISOString(),
//             },
//         });
        
//         return { clientSecret: paymentIntent.client_secret };   
//     }
    
