/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from 'src/auth/auth.module';
import { Payment, PaymentSchema,  } from 'src/schemas/payment.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { CarPost, CarPostSchema } from 'src/schemas/carpost.schema';
import { User, UserSchema } from 'src/schemas/user.schema';


@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: {
          expiresIn: configService.get<string | number>('JWT_EXPIRES')
        },
      }),
    }),
    AuthModule,
    HttpModule,
    MongooseModule.forFeature([{ name:Payment.name, schema:PaymentSchema}, { name: CarPost.name, schema: CarPostSchema }, { name: User.name, schema: UserSchema }]),

  ],
  providers: [PaymentService],
  controllers: [PaymentController],
})

export class PaymentModule {}
