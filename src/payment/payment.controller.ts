/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
/* eslint-disable prettier/prettier */
import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
// import { Cart } from './Cart.model';
import { PaymentService } from './payment.service';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';
import { Payment } from 'src/schemas/payment.schema';

@Controller('payment')
export class PaymentController {
        
    constructor (private readonly paymentService : PaymentService, private readonly jwtService: JwtService ,

    ) {}

    @UseGuards(AuthGuard('jwt'))
    @Post('add')
    async addPayment(@Body() body: { amount: string, postId: string }, @Req() req): Promise<any> {
        try {
            const userId = req.user.id; 
            const result = await this.paymentService.add(body.amount, userId, body.postId);
            return result;
        } catch (error) {
            console.log(error)
            throw new Error('Failed to add payment');
        }
    }

    
    @Post('/verify/:id')
    async verifyPayment(@Param('id') payment_id: string): Promise<any> {
        try {
            const result = await this.paymentService.verify(payment_id);
            return result;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Get('getPaymentPost/:postId')
    async findByPostId(@Param('postId') postId: string): Promise<Payment[]> {
        return await this.paymentService.findByPostId(postId);
    }

    @Get('getPayment/:userId')
    async findByUserId(@Param('userId') userId: string): Promise<Payment[]> {
        return await this.paymentService.findByUserId(userId);
    }

    
    // @Post('add')
    // async addPayment(@Body('amount') amount: string): Promise<any> {
    //     try {
    //     const result = await this.paymentService.add(amount);
    //     return result;
    //     } catch (error) {
    //     throw new Error('Failed to add payment');
    //     }
    // }
    
    // @UseGuards(AuthGuard('jwt'))
    // @Post('/create')
    // async checkout(@Body() body: { cart: Cart }) {
    //     try {
    //         if (!body.cart || !Array.isArray(body.cart) || body.cart.length === 0) {
    //             throw new Error('Invalid cart');
    //         }

    //         const result = await this.paymentService.checkout(body.cart);
    //         return result;
    //     } catch (error) {
    //         return { error: error.message };
    //     }
    // }

    // @UseGuards(AuthGuard('jwt'))
    // @Post('/create')
    // async checkout(@Body() body: { amount: number , postId: string ,userId: string , }) {
    //     try {
    //         if ( !body.amount || body.amount<= 0) {
    //             throw new Error('Invalid amount');
    //         }

    //         const result = await this.paymentService.checkout(body.amount,body.postId,body.userId);
    //         return result;
    //     } catch (error) {
    //         return { error: error.message };
    //     }
    // }

    // @Get('/clientSecret')
    // async getClientSecret() {
    //     try {
    //         const result = await this.paymentService.getClientSecret();
    //         return result;
    //     } catch (error) {
    //         return { error: error.message };
    //     }
    // }

    // async handlePayment(@Body() body: { amount: number; id: string }): Promise<{ message: string; success: boolean }> {
    //     const { amount, id } = body;
    //     return await this.paymentService.processPayment(amount, id);
    // }

    // @Get('clientSecret')
    // async getClientSecret() {
    //     try {
    //         const clientSecret = await this.paymentService.generateClientSecret();
    //         return { clientSecret };
    //     } catch (error) {
    //         console.error("Error generating client secret:", error);
    //         throw new Error('Internal server error');
    //     }
    // }
}


// import { Controller, Post, UseGuards } from '@nestjs/common';
// import { PaymentService } from './payment.service';
// import { AuthGuard } from '@nestjs/passport';
// import { JwtService } from '@nestjs/jwt';

// @Controller('payment')
// export class PaymentController {

//     constructor (private readonly paymentService : PaymentService, private readonly jwtService: JwtService ,) {}
        

//     @UseGuards(AuthGuard('jwt'))
//     @Post('create-checkout')
//     async createCheckoutSession(): Promise<string> {
//         return await this.paymentService.createCheckoutSession();
//     }
// }
