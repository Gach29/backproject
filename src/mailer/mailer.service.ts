/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import * as nodemailer from "nodemailer";

@Injectable()
export class MailerService {

    private async transporteur (){
        // const testAccount=await nodemailer.createTestAccount()
        const transport= nodemailer.createTransport({
            service:'gmail',
            host:"smtp.gmail.com",
            port:587,
            secure:false,
            ignoreTLS:true,
            auth : {
                user: process.env.USER,
                pass: process.env.APP_PASSWORD,
            },
        })

        return transport
    }

    async sendSignUpConfirmation(userEmail: string){
        (await this.transporteur()).sendMail({
            from : {
                name:' Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject:"Inscription",
            html: "<h3>Confirmation de votre inscription</h3>"
        });
    }


    async sendResetPassword(userEmail: string, url:string, code:string){
        (await this.transporteur()).sendMail({
            from : {
                name:' Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject:"Reset Password",
            html: `
            <a href="${url}?code=${code}">Réinitialiser le mot de passe</a>
            <p>Le code secret est <strong>${code}<strong></p>
            <p>Le code expirera dans 15 minutes</p>
            `,
        });
    }
    

    async demandExpertise(userEmail: string){
        (await this.transporteur()).sendMail({
            from : {
                name:' Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject:"Demande d'expertise",
            html: `
            <p> Vous avez reçu une demande d'expertise </p>
            <p> Checkez votre profil pour plus de details </p>              `,
        });
    }

    async demandAccepted(userEmail: string){
        (await this.transporteur()).sendMail({
            from : {
                name:' Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject:"Demande d'expertise pour votre voiture est acceptée",
            html: `
            <p> Une autre etape a faire </p>
            <p> Effectuer le paiement pour votre rapport d'expertise </p>              `,
        });
    }

    async demandRejected(userEmail: string){
        (await this.transporteur()).sendMail({
            from : {
                name:' Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject:"Demande d'expertise pour votre voiture est refusée",
            html: `
            <p> Nous regrettons de vous informer que votre demande a eté refusée </p>              `,
        });
    }

    async PaymentSent(userEmail: string, client: string , annonce : string){
        (await this.transporteur()).sendMail({
            from : {
                name:' Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject:" Paiement reçu ",
            html: `
            <p> Vous avez reçu un paiement de la part de ${client} pour votre annonce de ${annonce} </p>              `,
        });
    }

    async PaymentSuccess(userEmail: string, amount: string , annonce : string){
        if (!userEmail || !amount || !annonce) {
            console.error("Missing recipient email, amount or announcement");
            return;
        }
    
        (await this.transporteur()).sendMail({
            from : {
                name:'Flesk',
                address: process.env.USER,
            },
            to: userEmail,
            subject: "Paiement effectué",
            html: `
            <p>Vous avez effectué un paiement de ${amount} DT pour l'annonce de ${annonce}</p>
            `,
        });
    }
    
}
