/* eslint-disable prettier/prettier */
import { CarPost } from "src/schemas/carpost.schema";
import { Comment } from "src/schemas/comments.schema";
import { User } from "src/schemas/user.schema";

export class CommentCreatedEvent {
    constructor(public readonly comment: Comment, public readonly user: User, public readonly post: CarPost) {}
}
