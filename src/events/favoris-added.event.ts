/* eslint-disable prettier/prettier */
import { CarPost } from "src/schemas/carpost.schema";
import { User } from "src/schemas/user.schema";

export class FavorisAddedEvent {
    constructor(public readonly user: User, public readonly post: CarPost) {}
}
